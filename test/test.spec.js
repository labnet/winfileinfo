'use strict';

const winfileinfo = require('../');
const path = require('path');
const expect = require('expect.js');

describe('winfileinfo', function () {

    var info;
    before(function () {
        return winfileinfo(path.join(__dirname, 'sample.exe'))
            .then((obj) => {
                info = obj;
            });
    });

    it('#fileVersion', () => {
        expect(info.fileVersion).to.be('1.2.3.5000');
    });

    it('#productVersion', () => {
        expect(info.productVersion).to.be('1.0.0.1');
    });

    it('#fileType', () => {
        expect(info.fileType).to.be('app');
    });
});
