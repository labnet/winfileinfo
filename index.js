'use strict';

const native = require('./build/Release/winfileinfo');

function joinWords(word) {
    const a = (word >> 16) & 0xFFFF;
    const b = word & 0xFFFF;
    return [a, b].join('.');
}

/**
 * Returns information about a Windows file. The returned fields are:
 *
 * - string strucVersion
 * - string fileVersion
 * - string productVersion
 * - bool flagDebug
 * - bool flagInfoInferred
 * - bool flagPatched
 * - bool flagPrerelease
 * - bool flagPrivateBuild
 * - bool flagSpecialBuild
 * - string[] os
 * - string fileType
 * - string fileSubtype
 * - Date fileDate (not valid)
 *
 * @param {string} filename
 * @returns {Promise<Object>}
 */
module.exports = function winfileinfo(filename) {
    return native(filename)
        .then((info) => {
            const o = {};
            o.strucVersion = joinWords(info.dwStrucVersion);
            o.fileVersion = [joinWords(info.dwFileVersionMS), joinWords(info.dwFileVersionLS)].join('.');
            o.productVersion = [joinWords(info.dwProductVersionMS), joinWords(info.dwProductVersionLS)].join('.');

            const fileFlags = info.dwFileFlags & info.dwFileFlagsMask;
            if (fileFlags & 0x1) {
                o.flagDebug = true;
            }
            if (fileFlags & 0x10) {
                o.flagInfoInferred = true;
            }
            if (fileFlags & 0x4) {
                o.flagPatched = true;
            }
            if (fileFlags & 0x2) {
                o.flagPrerelease = true;
            }
            if (fileFlags & 0x8) {
                o.flagPrivateBuild = true;
            }
            if (fileFlags & 0x20) {
                o.flagSpecialBuild = true;
            }

            var os = [];
            if (info.dwFileOS == 0x0) {
                os.push('unknown');
            } else {
                if (info.dwFileOS & 0x10000) {
                    os.push('dos');
                }
                if (info.dwFileOS & 0x40000) {
                    os.push('nt');
                }
                if (info.dwFileOS & 0x1) {
                    os.push('windows-16');
                }
                if (info.dwFileOS & 0x4) {
                    os.push('windows-32');
                }
                if (info.dwFileOS & 0x20000) {
                    os.push('os2-16');
                }
                if (info.dwFileOS & 0x30000) {
                    os.push('os2-32');
                }
                if (info.dwFileOS & 0x2) {
                    os.push('pm-16');
                }
                if (info.dwFileOS & 0x3) {
                    os.push('pm-32');
                }
            }
            o.os = os;

            var fileType;
            switch (info.dwFileType) {
                case 0x1:
                    fileType = 'app';
                    break;
                case 0x2:
                    fileType = 'dll';
                    break;
                case 0x3:
                    fileType = 'drv';
                    break;
                case 0x4:
                    fileType = 'font';
                    break;
                case 0x7:
                    fileType = 'static-lib';
                    break;
                case 0x0:
                    fileType = 'unknown';
                    break;
                case 0x5:
                    fileType = 'vxd';
                    break;
                default:
                    throw new Error('unknown dwFileType: ' + info.dwFileType);
            }
            o.fileType = fileType;

            var fileSubtype = undefined;
            if (info.dwFileType == 0x3) {
                fileSubtype = null;
                switch (info.dwFileSubtype) {
                    case 0xA:
                        fileSubtype = 'comm';
                        break;
                    case 0x4:
                        fileSubtype = 'display';
                        break;
                    case 0x8:
                        fileSubtype = 'installable';
                        break;
                    case 0x2:
                        fileSubtype = 'keyboard';
                        break;
                    case 0x3:
                        fileSubtype = 'language';
                        break;
                    case 0x5:
                        fileSubtype = 'mouse';
                        break;
                    case 0x6:
                        fileSubtype = 'network';
                        break;
                    case 0x1:
                        fileSubtype = 'printer';
                        break;
                    case 0x9:
                        fileSubtype = 'sound';
                        break;
                    case 0x7:
                        fileSubtype = 'system';
                        break;
                    case 0xc:
                        fileSubtype = 'versioned-printer';
                        break;
                    case 0x0:
                        fileSubtype = 'unknown';
                        break;
                    default:
                        throw new Error('unknown dwFileSubtype: ' + info.dwFileSubtype);
                }
            } else if (info.dwFileType == 0x4) {
                fileSubtype = null;
                switch (info.dwFileSubtype) {
                    case 0x1:
                        fileSubtype = 'raster';
                        break;
                    case 0x3:
                        fileSubtype = 'truetype';
                        break;
                    case 0x2:
                        fileSubtype = 'vector';
                        break;
                    case 0x0:
                        fileSubtype = 'unknown';
                        break;
                    default:
                        throw new Error('unknown dwFileSubtype: ' + info.dwFileSubtype);
                }
            } else if (info.dwFileType == 0x5) {
                fileSubtype = info.dwFileSubtype;
            }
            if (typeof fileSubtype !== 'undefined') {
                o.fileSubtype = fileSubtype;
            }

            // note: this value can't be trusted
            const utc = Date.UTC(info.fileDateYear, info.fileDateMonth - 1, info.fileDateDay, info.fileDateHour, info.fileDateMinute, info.fileDateSecond, info.fileDateMillisecond);
            o.fileDate = new Date(utc);

            return o;
        });
};
