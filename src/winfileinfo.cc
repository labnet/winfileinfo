#include <nan.h>
#include <windows.h>

void HandleWinError(v8::Local<v8::Promise::Resolver> &resolver) {
	TCHAR lpBuffer[256];
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), lpBuffer, sizeof(lpBuffer) / sizeof(TCHAR) - 1, NULL);
	resolver->Reject(Nan::Error(lpBuffer));
}

void CreateObject(const Nan::FunctionCallbackInfo<v8::Value>& info) {
	auto resolver = v8::Promise::Resolver::New(info.GetIsolate());
	auto promise = resolver->GetPromise();
	info.GetReturnValue().Set(resolver);

	// Check argument
	auto filenameLocal = info[0];
	if (!filenameLocal->IsString()) {
		resolver->Reject(Nan::Error("missing filename"));
		return;
	}
	v8::String::Utf8Value filename(filenameLocal->ToString());

	// Get version info size
	DWORD versionInfoSize = GetFileVersionInfoSize(*filename, 0);
	if (!versionInfoSize) {
		HandleWinError(resolver);
		return;
	}
	auto data = new char[versionInfoSize];

	// Get version info
	DWORD handle = 0;
	if (!GetFileVersionInfo(*filename, handle, versionInfoSize, data)) {
		HandleWinError(resolver);
		delete data;
		return;
	}

	// Query version info
	LPVOID ptr;
	UINT size;
	if (!VerQueryValue(data, "\\", &ptr, &size)) {
		HandleWinError(resolver);
		delete data;
		return;
	}

	auto fileinfo = static_cast<VS_FIXEDFILEINFO *>(ptr);

	// Verify fileinfo signature
	if (fileinfo->dwSignature != 0xFEEF04BD) {
		resolver->Reject(Nan::Error("invalid file info signature"));
		delete data;
		return;
	}

	// Create object
	v8::Local<v8::Object> obj = Nan::New<v8::Object>();
	obj->Set(Nan::New("dwStrucVersion").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwStrucVersion)));
	obj->Set(Nan::New("dwFileVersionMS").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwFileVersionMS)));
	obj->Set(Nan::New("dwFileVersionLS").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwFileVersionLS)));
	obj->Set(Nan::New("dwProductVersionMS").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwProductVersionMS)));
	obj->Set(Nan::New("dwProductVersionLS").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwProductVersionLS)));
	obj->Set(Nan::New("dwFileFlagsMask").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwFileFlagsMask)));
	obj->Set(Nan::New("dwFileFlags").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwFileFlags)));
	obj->Set(Nan::New("dwFileOS").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwFileOS)));
	obj->Set(Nan::New("dwFileType").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwFileType)));
	obj->Set(Nan::New("dwFileSubtype").ToLocalChecked(), Nan::New(static_cast<uint32_t>(fileinfo->dwFileSubtype)));
	
	FILETIME filetime;
	filetime.dwLowDateTime = fileinfo->dwFileDateLS;
	filetime.dwHighDateTime = fileinfo->dwFileDateMS;

	SYSTEMTIME systime;
	if (!FileTimeToSystemTime(&filetime, &systime)) {
		HandleWinError(resolver);
		delete data;
		return;
	}

	obj->Set(Nan::New("fileDateYear").ToLocalChecked(), Nan::New(static_cast<uint32_t>(systime.wYear)));
	obj->Set(Nan::New("fileDateMonth").ToLocalChecked(), Nan::New(static_cast<uint32_t>(systime.wMonth)));
	obj->Set(Nan::New("fileDateDay").ToLocalChecked(), Nan::New(static_cast<uint32_t>(systime.wDay)));
	obj->Set(Nan::New("fileDateHour").ToLocalChecked(), Nan::New(static_cast<uint32_t>(systime.wHour)));
	obj->Set(Nan::New("fileDateMinute").ToLocalChecked(), Nan::New(static_cast<uint32_t>(systime.wMinute)));
	obj->Set(Nan::New("fileDateSecond").ToLocalChecked(), Nan::New(static_cast<uint32_t>(systime.wSecond)));
	obj->Set(Nan::New("fileDateMillisecond").ToLocalChecked(), Nan::New(static_cast<uint32_t>(systime.wMilliseconds)));

	resolver->Resolve(obj);
	delete data;
}

void Init(v8::Local<v8::Object> exports, v8::Local<v8::Object> module) {
	module->Set(Nan::New("exports").ToLocalChecked(), Nan::New<v8::FunctionTemplate>(CreateObject)->GetFunction());
}

NODE_MODULE(winfileinfo, Init)
